package com.demo.demo;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import com.demo.demo.model.LoginModel;
import com.demo.demo.model.Student;
import com.demo.demo.sync.APIClient;
import com.demo.demo.sync.APIInterface;
import com.demo.demo.utils.CommonFunction;
import com.demo.demo.utils.SubmitButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity  implements View.OnClickListener{
    private SubmitButton sBtnLoading;
    TextInputEditText username,pass;
    CoordinatorLayout body;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent=null;
        if(CommonFunction.stringToInt(CommonFunction.getShareDataString(getApplicationContext(),"type"))==2)
        intent=new Intent(Login.this,StudentForm.class);
        else if(CommonFunction.stringToInt(CommonFunction.getShareDataString(getApplicationContext(),"type"))==1)
            intent=new Intent(Login.this,MainActivity.class);
        if(intent!=null)
        {
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.activity_login);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sBtnLoading = (SubmitButton) findViewById(R.id.sbtn_loading);
        sBtnLoading.setOnClickListener(this);
        username=(TextInputEditText) findViewById(R.id.tie_username);
        pass=(TextInputEditText) findViewById(R.id.tie_password);
        body=(CoordinatorLayout)findViewById(R.id.cd_log_contant);
        TextInputLayout user=(TextInputLayout)findViewById(R.id.username);
        TextInputLayout pass=(TextInputLayout)findViewById(R.id.pass);
        TextView textView=(TextView)  findViewById(R.id.textView);
        ConstraintLayout layout=(ConstraintLayout)findViewById(R.id.cl_go_to_reg);
        Animation fadeOutAndroidAnimation;
        Animation move=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.buttonanim);
        fadeOutAndroidAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_animation);
        textView.startAnimation(fadeOutAndroidAnimation);
        layout.startAnimation(fadeOutAndroidAnimation);
        user.startAnimation(fadeOutAndroidAnimation);
        pass.startAnimation(fadeOutAndroidAnimation);
        sBtnLoading.startAnimation(fadeOutAndroidAnimation);



        ConstraintLayout goTologin=(ConstraintLayout)findViewById(R.id.cl_go_to_reg);
        goTologin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login.this,Register.class);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sbtn_loading:
                login();
                break;
        }

    }

public void login()
{
    try {
        String mUsername=username.getText().toString();
        String mPass=pass.getText().toString();
        if("".equals(mUsername))
        {
            username.setError("Enter Value");
            sBtnLoading.reset();
        }
        else if("".equals(mPass))
        {
            pass.setError("Enter Value");
            sBtnLoading.reset();
        }
        else{
            APIInterface apiInterface= APIClient.getClient().create(APIInterface.class);
            Call<LoginModel> call = apiInterface.logIn(mUsername,mPass);
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    if(response.body().getResult()==1)
                    {
                        Student student=response.body().getData().get(0);
                        Intent intent;
                        sBtnLoading.doResult(true);
                        if(student.getType()==1)
                            intent=new Intent(Login.this,MainActivity.class);
                        else
                            intent=new Intent(Login.this,StudentForm.class);
                        CommonFunction.setShareDataString(getApplication(),"name",student.getFname());
                        CommonFunction.setShareDataString(getApplication(),"username",student.getUsername());
                        CommonFunction.setShareDataString(getApplication(),"mob",student.getMob());
                        CommonFunction.setShareDataString(getApplication(),"up_st",student.getFile_up_status()+"");
                        CommonFunction.setShareDataString(getApplication(),"path",student.getFile_path());
                        CommonFunction.setShareDataString(getApplication(),"app_st",student.getApprove_st()+"");
                        CommonFunction.setShareDataString(getApplication(),"type",student.getType()+"");
                        CommonFunction.setShareDataString(getApplication(),"id",student.getId()+"");
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        username.setError(null);
                        pass.setError(null);
                        sBtnLoading.reset();
                       Toast.makeText(getApplicationContext(),"Invalid username or password!",Toast.LENGTH_LONG).show();
                    }



                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    username.setError(null);
                    pass.setError(null);
                    sBtnLoading.reset();
                    Toast.makeText(getApplicationContext(),"Invalid username or password!",Toast.LENGTH_LONG).show();
                }
            });
        }

    } catch (Exception e) {
        sBtnLoading.reset();
        Toast.makeText(getApplicationContext(),"Login failed",Toast.LENGTH_SHORT).show();
        e.printStackTrace();
    }
}

}
