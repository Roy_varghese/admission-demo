package com.demo.demo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.demo.demo.adapter.StudList;
import com.demo.demo.model.Student;
import com.demo.demo.sync.APIClient;
import com.demo.demo.sync.APIInterface;
import com.demo.demo.utils.ClickListner;

import java.net.URL;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    StudList adapter;
    RecyclerView list;
    public static MainActivity activity;
    private final Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        list=(RecyclerView)findViewById(R.id.stud_list) ;
        activity=this;
        adapter=new StudList(null, getApplicationContext(), new ClickListner() {
            @Override
            public void onClick(Student item, int position) {
                if(item.getFile_up_status()==0)
                    Toast.makeText(getApplicationContext(),"File Not Uploaded!",Toast.LENGTH_SHORT).show();
                else{
                    Intent intent=new Intent(MainActivity.this,ViewDoc.class);
                    DataClass.student=item;
                    DataClass.pos=position;
                    startActivity(intent);
                }

            }
        });
        list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        list.setAdapter(adapter);
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 0); // 20 second
    }
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            new syncLiveData().execute();
            handler.postDelayed(this, 10000); // 20 seconds
        }
    };
    private class syncLiveData extends AsyncTask<URL, Integer, Void> {
        protected Void doInBackground(URL... urls) {
            try {
                APIInterface apiInterface= APIClient.getClient().create(APIInterface.class);
                Call<List<Student>> call = apiInterface.getAllStudent();
                DataClass.list=call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateData();

        }


    }
    public void updateData()
    {
        adapter.updateList(DataClass.list);
    }

}
