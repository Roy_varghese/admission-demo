package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.demo.demo.model.LoginModel;
import com.demo.demo.model.Student;
import com.demo.demo.sync.APIClient;
import com.demo.demo.sync.APIInterface;
import com.demo.demo.utils.CommonFunction;
import com.demo.demo.utils.SubmitButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity implements View.OnClickListener{
    private SubmitButton sBtnLoading;
    TextInputEditText username,pass,name,mob;
    CoordinatorLayout body;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sBtnLoading = (SubmitButton) findViewById(R.id.sbtn_submit);
        sBtnLoading.setOnClickListener(this);
        username=(TextInputEditText) findViewById(R.id.td_reg_username);
        pass=(TextInputEditText) findViewById(R.id.td_reg_pass);
        name=(TextInputEditText)findViewById(R.id.td_reg_name);
        mob=(TextInputEditText)findViewById(R.id.te_reg_phone);
        body=(CoordinatorLayout)findViewById(R.id.cl_body);
        ConstraintLayout goTologin=(ConstraintLayout)findViewById(R.id.cl_goto_login);
        goTologin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Register.this,Login.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sbtn_submit:
                register();
                break;
        }
    }
    public void register()
    {
        try {
            String mUsername=username.getText().toString();
            String mPass=pass.getText().toString();
            String mPhone=mob.getText().toString();
            String mName=name.getText().toString();
            if("".equals(mUsername))
            {
                sBtnLoading.reset();
                username.setError("Enter valid data");
            }
            else if("".equals(mPass))
            {
                sBtnLoading.reset();
                pass.setError("Enter valid data");
            }
            else if("".equals(mPhone))
            {
                sBtnLoading.reset();
                mob.setError("Enter valid data");
            }
            else if("".equals(mName))
            {
                sBtnLoading.reset();
                name.setError("Enter valid data");
            }
            else{
                APIInterface apiInterface= APIClient.getClient().create(APIInterface.class);
                Call<LoginModel> call = apiInterface.register(mName,mUsername,mPass,mPhone);
                call.enqueue(new Callback<LoginModel>() {
                    @Override
                    public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                        if(response.body().getResult()==1)
                        {
                            Toast.makeText(getApplicationContext(),"Successfully Registered!",Toast.LENGTH_SHORT).show();
                            Intent intent;
                            sBtnLoading.doResult(true);

                            intent=new Intent(Register.this,StudentForm.class);
                            CommonFunction.setShareDataString(getApplication(),"name",name.getText().toString());
                            CommonFunction.setShareDataString(getApplication(),"username",username.getText().toString());
                            CommonFunction.setShareDataString(getApplication(),"mob",mob.getText().toString());
                            CommonFunction.setShareDataString(getApplication(),"up_st","0");
                            CommonFunction.setShareDataString(getApplication(),"path","");
                            CommonFunction.setShareDataString(getApplication(),"app_st","0");
                            CommonFunction.setShareDataString(getApplication(),"type","2");
                            Student student=response.body().getData().get(0);
                            CommonFunction.setShareDataString(getApplication(),"id",student.getId()+"");
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            sBtnLoading.reset();
                            Snackbar.make(body,"Invalid Data!",Snackbar.LENGTH_LONG).show();
                        }



                    }

                    @Override
                    public void onFailure(Call<LoginModel> call, Throwable t) {
                        sBtnLoading.reset();
                        Toast.makeText(getApplicationContext(),"Register failed",Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } catch (Exception e) {
            sBtnLoading.reset();
            Toast.makeText(getApplicationContext(),"Register failed",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
