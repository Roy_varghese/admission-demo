package com.demo.demo;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.demo.demo.model.LoginModel;
import com.demo.demo.model.Student;
import com.demo.demo.sync.APIClient;
import com.demo.demo.sync.APIInterface;
import com.demo.demo.utils.CommonFunction;
import com.demo.demo.utils.FilePath;
import com.demo.demo.utils.SubmitButton;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.net.URL;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentForm extends AppCompatActivity {
    MaterialButton select,name,phone;
    Uri selectedImage;
    File  destination;
    private SubmitButton upload;
    private final Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_form);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        name=(MaterialButton)findViewById(R.id.mb_user_name);
        phone=(MaterialButton)findViewById(R.id.mb_user_phone);
        name.setText(CommonFunction.getShareDataString(getApplication(),"name"));
        phone.setText(CommonFunction.getShareDataString(getApplication(),"mob"));
        upload = (SubmitButton) findViewById(R.id.sbtn_upload);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
            }
        });
        select=(MaterialButton)findViewById(R.id.mb_upload_file);
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (CommonFunction.isStoragePermissionGranted(getApplicationContext())==false)
                    ActivityCompat.requestPermissions(StudentForm.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                else
                    selectFile();

            }
        });
        checkStatus();
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 20000); // 20 second


    }

    public void selectFile()
    {

        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 100);



    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode==1&& CommonFunction.isStoragePermissionGranted(getApplicationContext()))
            selectFile();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            try {

                selectedImage= data.getData();
                upload.setVisibility(View.VISIBLE);
                select.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public void uploadFile() {
        //pass it like this
        try {
            String path = FilePath.getPath(StudentForm.this, selectedImage);
            File file = new File(path);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

// MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData("doc", file.getName(), requestFile);
            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            ;
            String id = CommonFunction.getShareDataString(getApplication(), "id");
            Call<LoginModel> call = apiInterface.upload(id, body);
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                    LoginModel loginModel = response.body();
                    if(loginModel.getResult()==1)
                    {
                        upload.doResult(true);
                        Toast.makeText(getApplicationContext(),"File Uploaded",Toast.LENGTH_SHORT).show();
                        select.setVisibility(View.INVISIBLE);
                        upload.setVisibility(View.INVISIBLE);
                        CommonFunction.setShareDataString(getApplicationContext(),"up_st","1");
                        checkStatus();

                    }

                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    String h = "";
                    upload.doResult(false);
                    upload.reset();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            upload.doResult(false);
            upload.reset();
        }
    }
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            new syncLiveData().execute();
            handler.postDelayed(this, 20000); // 20 seconds
        }
    };
        public void checkStatus()
        {
            int fileUp=CommonFunction.stringToInt(CommonFunction.getShareDataString(getApplication(),"up_st"));

            if(fileUp==1)
            {
                MaterialButton fildone=(MaterialButton) findViewById(R.id.mb_send_file);
                fildone.setVisibility(View.VISIBLE);
                MaterialButton file=(MaterialButton) findViewById(R.id.mb_upload_file);
                file.setVisibility(View.INVISIBLE);
                SubmitButton submit=(SubmitButton) findViewById(R.id.sbtn_upload);
                submit.setVisibility(View.INVISIBLE);
                int apprSt=CommonFunction.stringToInt(CommonFunction.getShareDataString(getApplication(),"app_st"));
                MaterialButton apprv=(MaterialButton) findViewById(R.id.mb_approve_st);
                if(apprSt==1)
                {
                    apprv.setText("Approved");
                    apprv.setIconResource(R.drawable.checkmark);
                }
                else
                {
                    apprv.setText("Approval Pending ");
                    apprv.setIconResource(R.drawable.hourglass);
                }
                apprv.setVisibility(View.VISIBLE);


            }

        }


    private class syncLiveData extends AsyncTask<URL, Integer, Void> {
        protected Void doInBackground(URL... urls) {
            try {
                int  id=CommonFunction.stringToInt(CommonFunction.getShareDataString(getApplicationContext(),"id"));
                APIInterface apiInterface= APIClient.getClient().create(APIInterface.class);
                Call<LoginModel> loginModelCall=apiInterface.checkApprove(id);
                Student student=loginModelCall.execute().body().getData().get(0);
                if(student.getApprove_st()==1)
                {
                    CommonFunction.setShareDataString(getApplication(),"app_st","1");
                    try {
                        handler.removeCallbacksAndMessages(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            checkStatus();
        }


    }

}
