package com.demo.demo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.demo.model.LoginModel;
import com.demo.demo.model.Student;
import com.demo.demo.sync.APIClient;
import com.demo.demo.sync.APIInterface;
import com.demo.demo.utils.SubmitButton;
import com.google.android.material.button.MaterialButton;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDoc extends AppCompatActivity {
    TextView name;
    SubmitButton submitButton;
    MaterialButton approved;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_doc);
        name=(TextView)findViewById(R.id.textView5);
        name.setText(DataClass.student.getFname());
        approved=(MaterialButton) findViewById(R.id.mb_approved);

        submitButton=(SubmitButton)findViewById(R.id.submitButton);
        if(DataClass.student.getApprove_st()==1)
        {
            approved.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
        }
        else{
            approved.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.VISIBLE);
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approve();
            }
        });

    }
    public void approve()
    {
        try {
            APIInterface apiInterface= APIClient.getClient().create(APIInterface.class);

            Call<LoginModel> call = apiInterface.setApprove(DataClass.student.getId());
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    int res=response.body().getResult();
                    if(res==1)
                    {
                        approved.setVisibility(View.VISIBLE);
                        submitButton.setVisibility(View.INVISIBLE);
                        if(DataClass.pos>-1)
                        {
                           DataClass.list.get(DataClass.pos).setApprove_st(1);
                            MainActivity.activity.updateData();
                        }

                    }
                    else
                    {
                        submitButton.reset();
                       Toast.makeText(getApplicationContext(),"Approval failed! Please try again",Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    submitButton.reset();
                    Toast.makeText(getApplicationContext(),"Approval failed! Please try again",Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            submitButton.reset();
            Toast.makeText(getApplicationContext(),"Approval failed! Please try again",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
