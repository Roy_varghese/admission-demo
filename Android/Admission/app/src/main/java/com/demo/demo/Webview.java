package com.demo.demo;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.demo.demo.sync.APIClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class Webview extends Fragment {


    public Webview() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            WebView mWebView=new WebView(getActivity());
            mWebView.getSettings().setJavaScriptEnabled(true);
            String path=APIClient.BASE_URL+"files/"+DataClass.student.getFile_path();
            mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url="+path);
            return mWebView;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }

}
