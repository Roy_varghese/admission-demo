package com.demo.demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.demo.R;
import com.demo.demo.model.Student;
import com.demo.demo.utils.ClickListner;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class StudList extends RecyclerView.Adapter<StudList.MyViewHolder> {
    List<Student> students;
    Context context;
    ClickListner clickListner;

    public StudList(List<Student> students, Context context, ClickListner clickListner) {
        this.students = students;
        this.context = context;
        this.clickListner = clickListner;
    }
    public void updateList(List<Student> list)
    {     this.students=list;
            notifyDataSetChanged();


    }
    @NonNull
    @Override
    public StudList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudList.MyViewHolder holder, int position) {
        Student student=students.get(position);
        try {
            holder.sl.setText((position+1)+"");
            holder.name.setText(student.getFname());
            if(student.getFile_up_status()==0)
            {
                holder.constraintLayout.setBackgroundResource(R.color.listRed);
                holder.imageView.setImageResource(R.drawable.ic_fiber_new_white);
            }
            if(student.getFile_up_status()==1)
            {
                holder.constraintLayout.setBackgroundResource(R.color.listYellow);
                holder.imageView.setImageResource(R.drawable.ic_attachment_white);
            }
            if(student.getApprove_st()==1)
            {
                holder.constraintLayout.setBackgroundResource(R.color.listGreen);
                holder.imageView.setImageResource(R.drawable.check_white);
            }
            holder.bind(student,position);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return (students==null)?0:students.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView sl,name;
        ImageView imageView;
        ConstraintLayout constraintLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
             sl=(TextView)itemView.findViewById(R.id.txt_sl);
            name=(TextView)itemView.findViewById(R.id.txt_user_name);
            imageView=(ImageView) itemView.findViewById(R.id.iv_appve);
            constraintLayout=(ConstraintLayout)itemView.findViewById(R.id.constraintLayout9);
        }
        public void bind(final Student student, final int pos)
        {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListner.onClick(student,pos);
                }
            });
        }
    }
}
