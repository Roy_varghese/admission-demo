package com.demo.demo.model;

import java.util.List;

public class LoginModel {
    int result;
    List<Student> data;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }
}
