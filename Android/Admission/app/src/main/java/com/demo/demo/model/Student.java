package com.demo.demo.model;

import java.io.Serializable;

public class Student implements Serializable {
    int id;
    String fname;
    String lname;
    String username;
    int type;
    String mob;
    int file_up_status;
    String file_path;
    int approve_st;

    public Student() {
    }

    public Student(String fname, String lname, String username, int type, String mob, int file_up_status, String file_path, int approve_st) {
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.type = type;
        this.mob = mob;
        this.file_up_status = file_up_status;
        this.file_path = file_path;
        this.approve_st = approve_st;
    }

    public String getFname() {
        return fname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public int getFile_up_status() {
        return file_up_status;
    }

    public void setFile_up_status(int file_up_status) {
        this.file_up_status = file_up_status;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public int getApprove_st() {
        return approve_st;
    }

    public void setApprove_st(int approve_st) {
        this.approve_st = approve_st;
    }
}
