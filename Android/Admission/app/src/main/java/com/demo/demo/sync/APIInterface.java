package com.demo.demo.sync;

import com.demo.demo.model.LoginModel;
import com.demo.demo.model.Student;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {


    @GET("getAlluser.php")
    Call<List<Student>> getAllStudent();

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginModel> logIn(@Field("username") String name, @Field("password") String pass);

    @FormUrlEncoded
    @POST("register.php")
    Call<LoginModel> register(@Field("fname") String name,
                                @Field("username") String user,@Field("password") String pass
                                ,@Field("mob") String mob);

    @Multipart
    @POST("uploadfile.php")
    Call<LoginModel> upload(
            @Part("id") String id,
            @Part MultipartBody.Part file );

    @FormUrlEncoded
    @POST("approve.php")
    Call<LoginModel> setApprove(@Field("id") int id);

    @FormUrlEncoded
    @POST("getApproveStatus.php")
    Call<LoginModel> checkApprove(@Field("id") int id);

}
