package com.demo.demo.utils;

import com.demo.demo.model.Student;

public interface ClickListner {
    void onClick(Student item, int position);
}
