package com.demo.demo.utils;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.text.format.DateFormat;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import androidx.core.content.ContextCompat;

public class CommonFunction {
    public static String timestampDateWithOutTime(String timestamp)
    {
        long time=0;
        try {
            if(timestamp.length()==10)timestamp=timestamp+"000";
            time=Long.parseLong(timestamp);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            time=0;
        }
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);

        try {
            return DateFormat.format("dd-MM-yyyy", cal).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }
    public static String timestampDate(String timestamp)
    {
        long time=0;
        try {
            if(timestamp.length()==10)timestamp=timestamp+"000";
            time=Long.parseLong(timestamp);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            time=0;
        }
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);

        try {
           return DateFormat.format("dd-MM-yyyy hh:mm a", cal).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";

        }
    }
    public static boolean isNetworkConnected(Context cnt) {
        ConnectivityManager cm = (ConnectivityManager) cnt.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    public static int stringToInt(String value) {
        int intValue=0;
        try {
            intValue=Integer.parseInt(value);
        } catch (Exception e) {
            intValue=0;
            e.printStackTrace();
        }
        return intValue;
    }
    public static long stringToLong(String value) {
        long longValue=0;
        try {
            longValue=Long.parseLong(value);
        } catch (Exception e) {
            longValue=0;
            e.printStackTrace();
        }
        return longValue;
    }
    public static float stringToFloat(String value) {
        float floatValue=0;
        try {
            floatValue=Float.parseFloat(value);
        } catch (Exception e) {
            floatValue=0;
            e.printStackTrace();
        }
        return floatValue;
    }
    public static LinkedTreeMap<String,String> stringToObject(String value)
    {
        LinkedTreeMap<String,String> linkedTreeMaps;
        try {
            linkedTreeMaps= (LinkedTreeMap<String,String>)(Object)value;
        } catch (Exception e) {
            linkedTreeMaps=new LinkedTreeMap<>();
            e.printStackTrace();
        }
        return linkedTreeMaps;
    }
    public static ArrayList<LinkedTreeMap<String,String>> stringToArray(Object value)
    {
        ArrayList<LinkedTreeMap<String,String>> linkedTreeMaps;
        try {
            linkedTreeMaps= (ArrayList<LinkedTreeMap<String,String>>)value;
        } catch (Exception e) {
            linkedTreeMaps=new ArrayList<>();
            e.printStackTrace();
        }
        return linkedTreeMaps;
    }

    public static String countDownTime(long startDate, long endDate) {
        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400
        //milliseconds
        long different = endDate - startDate;
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        return  elapsedDays+"d, "+elapsedHours+"h, "+elapsedMinutes+"m, "+elapsedSeconds+"s";

    }

    public  static  boolean getShareData(Context intent,String key){
        SharedPreferences settings = intent.getSharedPreferences("cricBang", 0);
        return settings.getBoolean(key, false);
    }
    public  static  void setShareData(Context intent,String key,boolean value){
        SharedPreferences sharedPrefs = intent.getSharedPreferences("cricBang", 0);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }
    public  static  String getShareDataString(Context intent,String key){
        try {
            SharedPreferences settings = intent.getSharedPreferences("cricBang", 0);
            return settings.getString(key,"");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  "";
    }
    public  static  void setShareDataString(Context intent,String key,String value){
        SharedPreferences sharedPrefs = intent.getSharedPreferences("cricBang", 0);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(key, value);
        editor.commit();

    }
    public static ArrayList<LinkedTreeMap<String,String>> sort(ArrayList<LinkedTreeMap<String,String>> linkedTreeMaps,String value,boolean descStatus)
    {
        try {
            for(int i=0;i<linkedTreeMaps.size();i++)
            {
                boolean swap=false;
                for(int j=0;j<linkedTreeMaps.size()-1;j++)
                {
                    long s1=stringToLong(linkedTreeMaps.get(j).get(value));
                    long s2=stringToLong(linkedTreeMaps.get(j+1).get(value));
                    LinkedTreeMap<String,String> map, map1;
                    if(descStatus==true&& s2>s1)
                    {swap=true;

                        map1=linkedTreeMaps.get(j+1);
                        map=linkedTreeMaps.get(j);
                        linkedTreeMaps.remove(j);
                        linkedTreeMaps.remove(j);
                        linkedTreeMaps.add(j,map1);
                        linkedTreeMaps.add(j+1,map);
                    }
                      else if(descStatus==false&& s2<s1)
                    {   swap=true;
                        map1=linkedTreeMaps.get(j+1);
                        map=linkedTreeMaps.get(j);
                        linkedTreeMaps.remove(j);
                        linkedTreeMaps.remove(j);
                        linkedTreeMaps.add(j,map1);
                        linkedTreeMaps.add(j+1,map);
                    }

                }
                if(!swap) break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return linkedTreeMaps;
    }
    public static boolean isStoragePermissionGranted(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ( ContextCompat.checkSelfPermission(c,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                return false;
            }
        }
        else {
            return true;
        }
    }
    public static LinkedTreeMap<String,String> getStringToLmap(String text)
    {
        try {
            Gson gson = new Gson();
            LinkedTreeMap<String,String> object = new LinkedTreeMap<>();
            gson.toJson(object); // May not serialize foo.value correctly
            LinkedTreeMap<String,String> map=gson.fromJson(text, object.getClass());
            return (LinkedTreeMap<String,String>) (Object)  map.get("result");


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
